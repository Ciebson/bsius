# README #


### Co to jest? ###

Projekt utworzony w Eclipse 3.8 (dostępnym w pracowni 013) zawierający implementację protokołu Diffie-Hellmana dla czatu grupowego (z osbługą wielu klientów) opartego na architekturze klient - serwer. Do wymiany informacji pomiędzy klientem, a serwerem wykorzystywany jest format JSON, a treść wiadmości jest szyfrowana przy użyciu zadanego algorytmu szyfrującego.

Kod wykorzystany do stworzenia samego czatu pochodzi ze strony: http://pirate.shu.edu (Creating a simple Chat Client/Server Solution)

### Uruchomienie serwera ###

1. Przechodzimy do wyeksportowanego pliku Server.jar (w głównym katalogu)
2. Uruchamiamy serwer komendą: java -jar Server.jar port (np. java -jar Server.jar 5000)
3. Uruchomiony poprawnie serwer powinien wyświetlić komunikat "Waiting for a client ..."

### Uruchomienie klienta ###

1. Przechodzimy do wyeksportowanego pliku Client.jar (w głównym katalogu)
2. Uruchamiamy klienta komendą: java -jar Client.jar address port encryptionType (np. java -jar Client.jar localhost 5000 cezar)
3. Uruchomiony poprawnie serwer powinien wyświetlić komunikat "Welcome to group chat! Say hi!" (Dodatkowo do testów komunikat ten podaje też klucz wyliczony dla klienta)

### Wymagania ###

* Java w wersji 1.8 (ze względu na bibliotekę używaną do kodowania Base64)

### Wady ###

* Jedyny dostępny algorytm szyfrowania to szyfr cezara (na XOR brakło mi czasu)
* Ze względu na niskie wartości liczb p i g oraz problem z generatorem liczb losowych wartość sekretu często powiela się dla kilku klientów odpalonych z rzędu. Nie udało mi się ustalić czegoś więcej.
* Brak testów jednostkowych.
* Ze względu na problemy z parserem musiałem w jsonie ustawić wartości liczbowe jako string.