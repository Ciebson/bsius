package Client;

import java.net.*;
import java.util.Random;
import java.io.*;
import org.json.*;

import Message.Crypting;
import Message.Message;
import java.util.Base64;

/**
 * Klasa klienta.
 * 
 * @author Hubert Ciebiada
 * 
 */
public class Client implements Runnable {
	Random generator = new Random();

	private Socket socket = null;
	private Thread thread = null;
	private DataInputStream console = null;
	private DataOutputStream streamOut = null;
	private ClientThread client = null;
	private volatile long p = -1, g = -1, a = generator.nextInt() % 30, A = -1,
			B = -1, s = -1;
	private String encryptionType = "none";

	public Client(String serverName, int serverPort, String encryptionType) {
		this.encryptionType = encryptionType;
		System.out.println("Establishing connection. Please wait ...");
		try {
			socket = new Socket(serverName, serverPort);
			System.out.println("Connected: " + socket);
			start();
		} catch (UnknownHostException uhe) {
			System.out.println("Host unknown: " + uhe.getMessage());
		} catch (IOException ioe) {
			System.out.println("Unexpected exception: " + ioe.getMessage());
		}
	}

	/*
	 * (non-Javadoc) Metoda nawiązująca połączenie z serwerem, wymianę wartości
	 * oraz obsługę wpisywanego tekstu.
	 */
	public void run() {
		while (thread != null && (p == -1 || g == -1)) {
			try {
				Message output = new Message();
				output.setRequest("keys");
				output.setEncryption(encryptionType);
				JSONObject jsonObject = new JSONObject(output);

				streamOut.writeUTF(jsonObject.toString());
				streamOut.flush();
				System.out.println("Key request sent");
				Thread.sleep(1000);
			} catch (IOException ioe) {
				System.out.println("Sending error: " + ioe.getMessage());
				stop();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		while (thread != null && A == -1) {
			try {
				A = (long) (Math.pow(g, a) % p);
				Message output = new Message();
				output.setA(Long.toString(A));

				JSONObject jsonObject = new JSONObject(output);
				streamOut.writeUTF(jsonObject.toString());
				streamOut.flush();
				System.out.println("A value sent");
				Thread.sleep(1000);
			} catch (IOException ioe) {
				System.out.println("Sending error: " + ioe.getMessage());
				stop();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		System.out
				.println("Welcome to group chat! Say hi! (secret: " + s + ")");
		// metoda obsługująca wpisywane wiadomości
		while (thread != null) {
			try {
				Message output = new Message();
				byte[] bytes = Crypting.encrypt(encryptionType,
						console.readLine(), s).getBytes("UTF-8");
				String encodedBase64 = Base64.getEncoder()
						.encodeToString(bytes);
				output.setText(encodedBase64);

				JSONObject jsonObject = new JSONObject(output);
				streamOut.writeUTF(jsonObject.toString());
				streamOut.flush();
			} catch (IOException ioe) {
				System.out.println("Sending error: " + ioe.getMessage());
				stop();
			}
		}
	}

	/**
	 * Metoda obsługująca wiadomości przychodzące.
	 * 
	 * @param input
	 */
	public void handle(String input) {
		JSONObject inputObject = null;
		try {
			inputObject = new JSONObject(input);
			if (inputObject.has("p") && inputObject.has("p")) {
				p = Long.parseLong(inputObject.get("p").toString());
				g = Long.parseLong(inputObject.get("g").toString());
			} else if (inputObject.has("b")) {
				B = Long.parseLong(inputObject.get("b").toString());
				s = (long) Math.pow(B, a) % p;
				System.out.println("B value received");
			} else if (inputObject.has("text")) {
				byte[] decodedBase64 = Base64.getDecoder().decode(
						inputObject.get("text").toString());

				String text = new String(decodedBase64);
				String decrypted = Crypting.decrypt(encryptionType, text, s);

				if (decrypted.equals(".bye")) {
					System.out.println("Good bye. Press RETURN to exit ...");
					stop();
				} else
					System.out.println(inputObject.get("sender").toString()
							+ ": " + decrypted);
			}
		} catch (JSONException e) {
			e.printStackTrace();
		}
	}

	public void start() throws IOException {
		console = new DataInputStream(System.in);
		streamOut = new DataOutputStream(socket.getOutputStream());
		if (thread == null) {
			client = new ClientThread(this, socket);
			thread = new Thread(this);
			thread.start();
		}
	}

	public void stop() {
		if (thread != null) {
			thread.stop();
			thread = null;
		}
		try {
			if (console != null)
				console.close();
			if (streamOut != null)
				streamOut.close();
			if (socket != null)
				socket.close();
		} catch (IOException ioe) {
			System.out.println("Error closing ...");
		}
		client.close();
		client.stop();
	}

	/**
	 * Klasa main.
	 * 
	 * @param args
	 *            0 - adres serwera, 1 - port, na którym uruchomiony jest
	 *            serwer, 2 - wybrane szyfrowanie
	 */
	public static void main(String args[]) {
		Client client = new Client(args[0], Integer.parseInt(args[1]), args[2]);
	}
}