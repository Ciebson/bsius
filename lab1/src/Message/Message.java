package Message;

/**
 * Klasa do przesyłania informacji pomiędzy serwerem a klientem.
 * @author Hubert Ciebiada
 *
 */
public class Message {
public String request;
public String p;
public String g;
public String A;
public String B;
public String text;
public String sender;
public String receiver;
public String encryption;


public String getEncryption() {
	return encryption;
}
public void setEncryption(String encryption) {
	this.encryption = encryption;
}
public String getSender() {
	return sender;
}
public void setSender(String sender) {
	this.sender = sender;
}
public String getReceiver() {
	return receiver;
}
public void setReceiver(String receiver) {
	this.receiver = receiver;
}
public String getText() {
	return text;
}
public void setText(String text) {
	this.text = text;
}
public String getB() {
	return B;
}
public void setB(String b) {
	B = b;
}
public String getRequest() {
	return request;
}
public void setRequest(String request) {
	this.request = request;
}
public String getP() {
	return p;
}
public void setP(String p) {
	this.p = p;
}
public String getG() {
	return g;
}
public void setG(String g) {
	this.g = g;
}
public String getA() {
	return A;
}
public void setA(String a) {
	A = a;
}

}
