package Server;

import java.net.*;
import java.util.Base64;
import java.util.HashMap;
import java.util.Random;
import java.io.*;

import org.json.*;

import Message.Crypting;
import Message.Message;

/**
 * Klasa serwera.
 * 
 * @author Hubert Ciebiada
 * 
 */
public class Server implements Runnable {
	Random generator = new Random();

	private ServerThread clients[] = new ServerThread[50];
	private HashMap<Integer, Long> clientKeys = new HashMap<Integer, Long>();
	private HashMap<Integer, Long> clientSecretKeys = new HashMap<Integer, Long>();
	private HashMap<Integer, String> clientsEncryption = new HashMap<Integer, String>();
	private ServerSocket server = null;
	private Thread thread = null;
	private int clientCount = 0;
	private volatile long p = 23, g = 5, b = generator.nextInt() % 30, B = -1,
			s = -1;

	/**
	 * Konstruktor klasy serwera.
	 * 
	 * @param port
	 *            Port, na którym zostanie uruchomiony serwer.
	 */
	public Server(int port) {
		try {
			System.out
					.println("Binding to port " + port + ", please wait  ...");
			server = new ServerSocket(port);
			System.out.println("Server started: " + server);
			start();
		} catch (IOException ioe) {
			System.out.println(ioe);
		}
	}

	public void run() {
		while (thread != null) {
			try {
				System.out.println("Waiting for a client ...");
				addThread(server.accept());
			} catch (IOException ie) {
				System.out.println("Acceptance Error: " + ie);
			}
		}
	}

	private int findClient(int ID) {
		for (int i = 0; i < clientCount; i++)
			if (clients[i].getID() == ID)
				return i;
		return -1;
	}

	/**
	 * Metoda obsługująca wiadomości przychodzące do serwera.
	 * 
	 * @param ID
	 *            ID nadawcy.
	 * @param input
	 *            JSON z wiadomością.
	 */
	public synchronized void handle(int ID, String input) {
		JSONObject inputObject = null;
		try {
			inputObject = new JSONObject(input);

			// obsługa żądania o wartości p i g
			if (inputObject.has("request")) {
				if (inputObject.has("encryption")) {
					clientsEncryption.put(ID, inputObject.get("encryption")
							.toString());
				}
				if (inputObject.get("request").equals("keys")) {
					Message output = new Message();
					output.setP(Long.toString(p));
					output.setG(Long.toString(g));
					JSONObject outputObject = new JSONObject(output);
					clients[findClient(ID)].send(outputObject.toString());
				}
			}
			// odebranie wartości A od klienta, wyliczenie i przekazanie
			// wartości B
			else if (inputObject.has("a")) {
				long tempA = Long.parseLong(inputObject.get("a").toString());
				if (tempA != -1) {
					clientKeys.put(ID, tempA);
					B = (long) (Math.pow(g, b) % p);
					s = (long) Math.pow(tempA, b) % p;
					clientSecretKeys.put(ID, s);

					Message output = new Message();
					output.setB(Long.toString(B));

					JSONObject outputObject = new JSONObject(output);
					clients[findClient(ID)].send(outputObject.toString());
				}
			}
			// obsługa tekstu
			else if (inputObject.has("text")) {
				if (inputObject.get("text").toString().equals(".bye")) {
					clients[findClient(ID)].send(".bye");
					remove(ID);
				} else {
					for (int i = 0; i < clientCount; i++) {
						// wysyłane do wszystkich klientów oprócz nadawcy
						if (!clients[i].equals(clients[findClient(ID)])) {
							Message output = new Message();

							byte[] decodedBase64 = Base64.getDecoder().decode(
									inputObject.get("text").toString());
							String text = new String(decodedBase64);

							String decrypted = Crypting.decrypt(
									clientsEncryption.get(ID), text,
									clientSecretKeys.get(ID));

							// kodowanie zgodne z tym ustalonym z klientem
							String encrypted = Crypting.encrypt(
									clientsEncryption.get(clients[i].getID()),
									decrypted,
									clientSecretKeys.get(clients[i].getID()));

							byte[] bytes = encrypted.getBytes("UTF-8");
							String encodedBase64 = Base64.getEncoder()
									.encodeToString(bytes);

							output.setText(encodedBase64);
							output.setSender(Integer.toString(ID));

							JSONObject outputObject = new JSONObject(output);
							clients[i].send(outputObject.toString());
						}
					}
				}
			}
		} catch (JSONException e) {
			e.printStackTrace();
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
	}

	public synchronized void remove(int ID) {
		int pos = findClient(ID);
		if (pos >= 0) {
			ServerThread toTerminate = clients[pos];
			System.out.println("Removing client thread " + ID + " at " + pos);
			if (pos < clientCount - 1)
				for (int i = pos + 1; i < clientCount; i++)
					clients[i - 1] = clients[i];
			clientCount--;
			try {
				toTerminate.close();
			} catch (IOException ioe) {
				System.out.println("Error closing thread: " + ioe);
			}
			toTerminate.stop();
		}
	}

	private void addThread(Socket socket) {
		if (clientCount < clients.length) {
			System.out.println("Client accepted: " + socket);
			clients[clientCount] = new ServerThread(this, socket);
			try {
				clients[clientCount].open();
				clients[clientCount].start();
				clientCount++;
			} catch (IOException ioe) {
				System.out.println("Error opening thread: " + ioe);
			}
		} else
			System.out.println("Client refused: maximum " + clients.length
					+ " reached.");
	}

	public void start() {
		if (thread == null) {
			thread = new Thread(this);
			thread.start();
		}
	}

	public void stop() {
		if (thread != null) {
			thread.stop();
			thread = null;
		}
	}

	/**
	 * Metoda main.
	 * 
	 * @param args
	 *            Argumenty: 0 - port, na którym zostanie uruchomiony serwer
	 */
	public static void main(String args[]) {
		Server server = new Server(Integer.parseInt(args[0]));
	}

}
