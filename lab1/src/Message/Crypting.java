package Message;

/**
 * Klasa z metodami do szyfrowania.
 * @author Hubert Ciebiada
 *
 */
public class Crypting {
	
	/**
	 * Zaszyfrowuje zadany tekst.
	 * @param encryptionType Typ kodowania.
	 * @param sentence Tekst do zaszyfrowania.
	 * @param k Klucz.
	 * @return
	 */
	public static String encrypt(String encryptionType, String sentence, Long k){
		if(encryptionType.equals("cezar")){
			return cesarEncrypt(sentence, k);
		}
		else if (encryptionType.equals("none"))
			return sentence;
		else
			return sentence;
	}
	
	/**
	 * Odszyfrowuje zadany tekst.
	 * @param encryptionType Typ kodowania.
	 * @param sentence Tekst do odkowania.
	 * @param k Klucz.
	 * @return Odkodowany tekst.
	 */
	public static String decrypt(String encryptionType, String sentence, Long k){
		if(encryptionType.equals("cezar")){
			return cesarDecrypt(sentence, k);
		}
		else if (encryptionType.equals("none"))
			return sentence;
		else
			return sentence;
	}
	
	
	/**
	 * Zaszyfrowuje szyfrem cezara o zadanym kluczu.
	 * @param sentence Tekst do zakodowania.
	 * @param k Klucz przesunięcia.
	 * @return Zakodowany tekst.
	 */
	private static String cesarEncrypt(String sentence, Long k) {
		String s = "";
		for (int i = 0; i < sentence.length(); i++) {
			char c = (char) (((sentence.charAt(i) + k)));
			s += c;
		}
		return s;
	}
	
	/**
	 * Odszyfrowuje szyfr cezara.
	 * @param sentence Tekst do odkodowania.
	 * @param k Klucz przesunięcia.
	 * @return Odkodowany tekst.
	 */
	private static String cesarDecrypt(String sentence, Long k) {
		String s = "";
		for (int i = 0; i < sentence.length(); i++) {
			char c = (char) (((sentence.charAt(i) - k)));
			s += c;
		}
		return s;
	}
}
